
import java.io.Console;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;
public class Program {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/testdb?useSSL=false";
        String user = "jeffrey";
        String password = "";
        
        Console console = System.console();
        //char[] passwordStr;
        String query = "SELECT VERSION()";

        
        Command commands = null;//new Command("shell");
        
        boolean login = true;
        while(login)
        {
        	Scanner sc = new Scanner(System.in); 

        	System.out.print("login as: ");
        	user = sc.nextLine();
        	System.out.print("password: ");
        	char[] passwordStr  = console.readPassword();
        	password = new String(passwordStr);
        	
        	try (Connection con = DriverManager.getConnection(url, user, password);
                    Statement st = con.createStatement();
                    ResultSet rs = st.executeQuery(query)) {

                    if (rs.next()) {
                        
                        System.out.println("Database powered by " +  rs.getString(1));
                    }
                    System.out.println();
                    
                    boolean run = true;
                    while(run)
                    {
                    	System.out.print("shell> ");
                    	sc = new Scanner(System.in); 
                    	String name = sc.nextLine();
                    	if(name.equals("exit"))
                    		run = false;
                    	else
                    	{
                    		System.out.println(name);
                    		if(commands == null)
                    		{
                    			commands = new Command(name,null);
                    			ExecuteCommand(name);
                    		}
                    		else	
                    		{
                    			AddCommand(commands,name);
                    			ExecuteCommand(name);
                    		}
                    		
                    		
                    	}	
                    }
                    
                    login = false;
                    System.out.println("\nExiting");
                    ShowHistory(commands);

                } catch (SQLException ex) {
                    System.out.println("ERROR Complete Login");
                    Logger lgr = Logger.getLogger(Program.class.getName());
                    lgr.log(Level.SEVERE, ex.getMessage(), ex);
                } 
        	
        }
        
        

	}
	
	private static void ExecuteCommand(String command)
	{
		String[] parts = command.split(" ");
		if(parts[0].equals(""))
		{
			System.out.println("You did not type anything..");
		}
		else
		{
			String command_exec = parts[0]; 
			String[] params = getParams(parts);
			System.out.println("You typed: "+command_exec);
			if(params != null)
				for(int i=0;i<params.length;i++)
					System.out.println("Param ["+i+"]: "+params[i]);
			else
				System.out.println("no params");
		
			switch(command_exec)
			{
				case "clear":
					for (int i = 0; i < 48; ++i) System.out.println();
					break;
			}
		
		}
		
	}
	
	private static String[] getParams(String[] command)
	{
		if(command == null)
			return null;
		
		String[] params = new String[command.length-1];
		if(command.length == 1)
			return null;
		
		if(command.length == 2)
		{
			params[0]=command[1];
			return params;
		}
		
		for(int i=0; i<command.length-1;i++)
			params[i] = command[i+1];
		return params;
	}
	
	private static void AddCommand(Command linkedList, String new_command) {
		Command list = linkedList;
		while(list.GetNext() != null)
		{
			list = list.GetNext();
		}
		list.SetNext(new Command(new_command,null));
	}
	private static void ShowHistory(Command commands)
	{
		int i = 0;
		while(commands != null)
		{
			System.out.println("["+i+"]: " + commands.GetInfo());
			commands = commands.GetNext();
			i++;
		}
	}
	
	public final static void clearConsole()
	{
	    try
	    {
	        final String os = System.getProperty("os.name");

	        if (os.contains("Windows"))
	        {
	            Runtime.getRuntime().exec("cls");
	        }
	        else
	        {
	            Runtime.getRuntime().exec("clear");
	        }
	    }
	    catch (final Exception e)
	    {
	    	Logger lgr = Logger.getLogger(Program.class.getName());
	    	lgr.log(Level.SEVERE, e.getMessage(), e);
	    }
	}
}
